# Chrome registry entry
### execute the following command to add it to current user
REG ADD "HKCU\Software\Google\Chrome\NativeMessagingHosts\io.universis.grades_sign" /ve /t REG_SZ /d "C:\UniverSIS\GradesDigitalSigning\chrome_native_messaging_manifest.json" /f

or

###.reg file
Windows Registry Editor Version 5.00
[HKEY_CURRENT_USER\Software\Google\Chrome\NativeMessagingHosts\io.universis.grades_sign]
@="C:\\UniverSIS\\GradesDigitalSigning\\chrome_native_messaging_manifest.json"

### execute the following command to add it to all users
REG ADD "HKLM\Software\Google\Chrome\NativeMessagingHosts\io.universis.grades_sign" /ve /t REG_SZ /d "C:\UniverSIS\GradesDigitalSigning\chrome_native_messaging_manifest.json" /f

or

#.reg file
Windows Registry Editor Version 5.00
[HKEY_LOCAL_MACHINE\Software\Google\Chrome\NativeMessagingHosts\io.universis.grades_sign]
@="C:\\UniverSIS\\GradesDigitalSigning\\chrome_native_messaging_manifest.json"


# Firefox registry entry
### execute the following command to add it to current user
REG ADD "HKCU\Software\Mozilla\NativeMessagingHosts\io.universis.grades_sign" /ve /t REG_SZ /d "C:\UniverSIS\GradesDigitalSigning\firefox_native_messaging_manifest.json" /f

or

#.reg file
Windows Registry Editor Version 5.00
[HKEY_CURRENT_USER\Software\Mozilla\NativeMessagingHosts\io.universis.grades_sign]
@="C:\\UniverSIS\\GradesDigitalSigning\\firefox_native_messaging_manifest.json"


### execute the following command to add it to all users
REG ADD "HKLM\Software\Mozilla\NativeMessagingHosts\io.universis.grades_sign" /ve /t REG_SZ /d "C:\UniverSIS\GradesDigitalSigning\firefox_native_messaging_manifest.json" /f

or

#.reg file
Windows Registry Editor Version 5.00
[HKEY_LOCAL_MACHINE\Software\Mozilla\NativeMessagingHosts\io.universis.grades_sign]
@="C:\\UniverSIS\\GradesDigitalSigning\\firefox_native_messaging_manifest.json"