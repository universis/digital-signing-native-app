window.addEventListener("message", function(event) {
  // We only accept messages from ourselves
  if (event.source != window)
    return;

  if (event.data.type && (event.data.type == "UNIVERSIS")) {
    console.log("Content script received: " + event.data.text);
    browser.runtime.sendMessage('grade_signing@universis.io','hello');
  }
}, false);