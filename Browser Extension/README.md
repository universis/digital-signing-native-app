## UniverSIS Grade Signing Browser Extension

[Release v1](https://gitlab.com/universis/digital-signing-native-app/-/releases/v1)

Tο extension για λόγους development μπορεί να τοποθετηθεί σε ενα folder τοπικά και:
* στον chrome στο chrome://extensions να φορτωθεί απο το "Load Unpacked extension..."
* στον firefox στο about:debugging#/runtime/this-firefox απο το "Load Temporary Add-on"

Debugging tools:
Στο console της σελιδας και του plugin και ειδικα στον firefox στο console του browser: https://developer.mozilla.org/en-US/docs/Tools/Browser_Toolbox.

### Προαπαιτούμενα
#### Γενικα:
* Πρεπει να υπαρχει το `GradesDigitalSigning.exe` καπου στο μηχανημα μας.
* Πρεπει να εχουν εγκατασταθει οι driver των κρυπτοσυσκευων.
* Πρεπει να εχει ρυθμιστει το Native Messaging αναλογα με το λειτουργικο συστημα που εχουμε.
* Ειδικα για το chrome extension πρεπει το extension id να περαστει στο `contentScript.js` και στο `chrome_native_messaging_manifest.json`. Το id μπορουμε να το βρουμε οταν κανουμε development, αφου κανουμε load unpacked, στο `chrome://extensions/`.
* Πρεπει στο `manifest.json`, στα keys content_scripts->matches και externally_connectable->matches να βαλουμε τα url των σελιδων που θελουμε να δουλευει το extension

#### Windows
* Μπορουμε να χρησιμοποιησουμε το `GradesDigitalSigningInstaller.exe`, που κανουμε build με το project που βρίσκεται στο https://repo.it.auth.gr/SERV/native-signing-app-installer. Ο installer περιεχει τους driver για τις κρυπτοσυσκευες και φτιαχνει τα απαραιτητα registry entries.
Εναλλακτικα πρεπει:
* να εγκαταστησουμε χειροκίνητα τους driver των κρυπτοσυσκευων.
* να βαλουμε στο μηχανημα μας τα δυο native messaging manifests που υπαρχουν στο φακελο `windows prerequsites`.
* να φτιαξουμε τα registry entries που περιγραφονται στο `windows prerequsites\Registry entries.md` με το path να δειχνει στα native messaging manifests.
* να αλλαξουμε το `path` στα native messaging manifests ωστε να δειχνει στο `GradesDigitalSigning.exe`.
* Ειδικα για το chrome extension πρεπει το extension id να περαστει στο `contentScript.js` και στο `chrome_native_messaging_manifest.json`. Το id μπορουμε να το βρουμε οταν κανουμε development, αφου κανουμε load unpacked, στο `chrome://extensions/`.
* Πρεπει στο `manifest.json`, στα keys content_scripts->matches και externally_connectable->matches να βαλουμε τα url των σελιδων που θελουμε να δουλευει το extension

#### Mac και Linux
Πρεπει:
* να εγκαταστησουμε χειροκίνητα τους driver των κρυπτοσυσκευων.
* να βαλουμε στο μηχανημα μας τα δυο native messaging manifests που υπαρχουν στο φακελο `windows prerequsites` στην σωστη τοποθεσια, οπως περιγραφεται [εδω](https://developer.chrome.com/apps/nativeMessaging#native-messaging-host-location).
* να αλλαξουμε το `path` στα native messaging manifests ωστε να δειχνει στο `GradesDigitalSigning` executable.
* Ειδικα για το chrome extension πρεπει το extension id να περαστει στο `contentScript.js` και στο `chrome_native_messaging_manifest.json`. Το id μπορουμε να το βρουμε οταν κανουμε development, αφου κανουμε load unpacked, στο `chrome://extensions/`.
* Πρεπει στο `manifest.json`, στα keys content_scripts->matches και externally_connectable->matches να βαλουμε τα url των σελιδων που θελουμε να δουλευει το extension


Με κάθε publish το version στο manifest.json πρεπει να αυξάνεται.
