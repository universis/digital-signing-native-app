<img src="https://fedoraproject.org/w/uploads/2/2d/Logo_fedoralogo.png" alt="Fedora">


# Getting Started
The following steps will get you up-and-running with the Native Signing application

First, do a general update of the packages you have installed
*  `sudo dnf makecache && sudo dnf update`

Now, lets install the dependencies 
*  `sudo dnf install @development-tools libusb-devel libusb pcsc-lite-ccid opensc pcsc-lite pcsc-lite-devel`

Here we are downloading the LuxTrust middleware to install Gemalto drivers and the
middleware itself
*  `wget https://www.luxtrust.lu/downloads/middleware/LuxTrust_Middleware_1.2.1_Fedora_64bit.tar.gz` 

Decompress the file
*  `tar -xvf LuxTrust_Middleware_1.2.1_Fedora_64bit.tar.gz`

And now install the rpm files

*  `sudo dnf install ./Gemalto_Middleware_Fedora_64bit_7.2.0-b04.rpm`

*  `sudo dnf install ./luxtrust-middleware-1.2.1-64.rpm`


## Development Guides

In order to develop and test the native app, you need a different setup and some 
more dependencies

*  `sudo dnf install python3-pip` (most likely already installed)
*  `sudo dnf install @development-tools gcc python3-devel python3-setuptools swig python3-tkinter`
*  `pip3 install --user pipenv`
*  `echo export PATH="$HOME/.local/bin:$PATH" >> ~/.bashrc`
*  `source ~/.bashrc`

Make sure to be in digital-signing-native-app/Native App/
*  `cp ./LinuxPipfile.lock ./Pipfile.lock`
*  `pipenv sync`

Notes / Troubleshooting
*  If syncing the dependencies fails, make sure you have python3 as a default 
python version. 

* If you are running Fedora 32, where python3.8 is the default, 
and the syncing fails try installing python3.6 and making that your default
```bash
sudo dnf install python36
alias python=python3.6
source ~/.bashrc
pipenv --rm 
pipenv sync
```
