<img src="https://assets.ubuntu.com/v1/8dd99b80-ubuntu-logo14.png" alt="Ubuntu">

# Getting Started
The following steps will get you up-and-running with the Native Signing application

First, do a general update of the packages you have installed
*  `sudo apt-get update && sudo apt-get upgrade`

Now, lets install the dependencies 
*  `sudo apt-get install build-essential libusb-dev libusb++-0.1-4 libccid pcscd libpcsclite1 libpcsclite-dev`

Here we are downloading and installing some dependencies of Gemalto drivers that aren't available in 
the repositories of the latest updates of Ubuntu
*  `wget http://ftp.us.debian.org/debian/pool/main/g/glibc/multiarch-support_2.28-10_amd64.deb`

*  `sudo dpkg -i ./multiarch-support_2.28-10_amd64.deb`

*  `wget http://security.debian.org/debian-security/pool/updates/main/o/openssl/libssl1.0.0_1.0.1t-1+deb8u12_amd64.deb`

*  `sudo dpkg -i ./libssl1.0.0_1.0.1t-1+deb8u12_amd64.deb`


Here we are downloading the LuxTrust middleware to install Gemalto drivers and the
middleware itself
*  `wget https://www.luxtrust.lu/downloads/middleware/LuxTrust_Middleware_1.2.1_Ubuntu_64bit.tar.gz`

Decompress the file
*  `tar -xvf ./LuxTrust_Middleware_1.2.1_Ubuntu_64bit.tar.gz`

Here we are installing some last dependencies

*  `sudo apt-get install libncurses5 libqt5core5a libqt5gui5 libqt5widgets5 libtinfo5`

And now install the deb files

*  `sudo dpkg -i  ./Gemalto_Middleware_Ubuntu_64bit_7.2.0-b04.deb`

*  `sudo dpkg -i ./luxtrust-middleware-1.2.1-64.deb`


## Development Guides

In order to develop and test the native app, you need a different setup and some 
more dependencies

*  `sudo apt-get install python3-pip` (most likely already installed)
*  `sudo apt install build-essential gcc python3-dev python3-setuptools swig python3-tk`
*  `pip3 install --user pipenv`
*  `echo export PATH="$HOME/.local/bin:$PATH" >> ~/.bashrc`
*  `source ~/.bashrc`

Make sure to be in digital-signing-native-app/Native App/
*  `cp ./LinuxPipfile.lock ./Pipfile.lock`
*  `pipenv sync`
