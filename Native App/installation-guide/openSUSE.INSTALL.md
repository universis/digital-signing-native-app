<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d0/OpenSUSE_Logo.svg/500px-OpenSUSE_Logo.svg.png" alt="openSUSE">

# Getting Started
The following steps will get you up-and-running with the Native Signing application

First, do a general update of the packages you have installed
*  `sudo zypper refresh && sudo zypper update`

Now, lets install the dependencies 
*  `sudo zypper install --type pattern devel_basis`
*  `sudo zypper install libusb-devel libusb pcsc-ccid opensc pcsc-lite pcsc-lite-devel`

Here we are downloading the LuxTrust middleware to install Gemalto drivers and the
middleware itself
*  `wget https://www.luxtrust.lu/downloads/middleware/LuxTrust_Middleware_1.2.1_Opensuse_64bit.tar.gz` 

Decompress the file
*  `tar -xvf ./LuxTrust_Middleware_1.2.1_Opensuse_64bit.tar.gz`

Install some dependencies of Gemalto drivers
*  `sudo zypper install libQt5Core5 libQt5Gui5 libQt5Widgets5 libncurses5`

*  `sudo zypper in libopenssl1_0_0`

Create a soft-link of `libcrypto`
*  `sudo ln -s /usr/lib64/libcrypto.so.1.0.0 libcrypto.so.1.0.0`

In openSUSE Leap run:
*  `sudo zypper install libqt4-x11 libqt4-devel`

In openSUSE tumbleweed run:
*  `sudo zypper addrepo https://download.opensuse.org/repositories/KDE:/Qt/openSUSE_Factory/ KDE &&
 sudo zypper install libqt4 libqt4-devel`

And now install the rpm files

*  `sudo rpm -i ./Gemalto_Middleware_opensuse42.1_64bit_7.2.0-b04.rpm`

*  `sudo rpm -i ./luxtrust-middleware-1.2.1-64.rpm`


## Development Guides

In order to develop and test the native app, you need a different setup and some 
more dependencies

*  `sudo zypper install python3-pip` (most likely already installed)
*  `sudo zypper install --type pattern devel_basis`
*  `sudo zypper install gcc python3-devel python3-setuptools swig python3-tk`
*  `pip3 install --user pipenv`
*  `echo export PATH="$HOME/.local/bin:$PATH" >> ~/.bashrc`
*  `source ~/.bashrc`

Make sure to be in digital-signing-native-app/Native App/
*  `cp ./LinuxPipfile.lock ./Pipfile.lock`
*  `pipenv sync`
