import unittest
import gc, time
from PyKCS11 import PyKCS11Error, CKR_PIN_INCORRECT, CKR_USER_ALREADY_LOGGED_IN
from src.TokenManager import TokenManager
from src.LogicException import LogicException

def file_callback(loaded, fileTypes, pcks11):
    return loaded

class TestTokenManager(unittest.TestCase):
    def setUp(self):
        gc.collect()
        self.tm = TokenManager(file_callback)
        self.tokens_len = 1
        self.slot_serial = '021f60d0'
        self.pin = 'admin'
        self.cert_id = '228.10.12.161.68.68.63.246'
        # IMPORTANT START
        # THIS IS BECAUSE GC is not fast enough
        # the sessions do not close as a result
        # we see no devices
        time.sleep(0.5)
        # MODIFY ACCORDING NEEDS
        # IMPORTANT END

    def tearDown(self):
        gc.collect()

    def test_list_slots(self):
        result = self.tm.list_slots()
        # test length == 1
        self.assertEqual(len(result), self.tokens_len)
        # test if slot_serial in dict
        self.assertIn(self.slot_serial, result)

    def test_open_session(self):
        result = self.tm.list_slots()
        self.assertFalse(result[self.slot_serial]['open'])
        self.tm.open_session(self.slot_serial)
        result = self.tm.list_slots()
        self.assertTrue(result[self.slot_serial]['open'])

    def test_invalid_slot_serial(self):
        try:
            self.tm.open_session('404')
            self.assertEqual(1, 0)
        except LogicException as ex:
            self.assertEqual(str(ex), 'Incorrect slot serial number.')

    def test_login_wrong_password(self):
        self.tm.open_session(self.slot_serial)
        try:
            self.tm.login_session(self.slot_serial, 'wrong password')
            self.assertEqual(1, 0)
        except PyKCS11Error as ex:
            self.assertEqual(ex.value, CKR_PIN_INCORRECT)

    def test_login(self):
        self.tm.open_session(self.slot_serial)
        result = self.tm.list_slots()
        self.assertEqual(result[self.slot_serial]['open'], True)
        self.assertEqual(result[self.slot_serial]['logged'], False)
        self.tm.login_session(self.slot_serial, self.pin)
        result = self.tm.list_slots()
        self.assertEqual(result[self.slot_serial]['open'], True)
        self.assertEqual(result[self.slot_serial]['logged'], True)

    def test_login_logged_in(self):
        self.tm.open_session(self.slot_serial)
        try:
            self.tm.login_session(self.slot_serial, self.pin)
            self.tm.login_session(self.slot_serial, self.pin)
            self.assertEqual(1, 0)
        except PyKCS11Error as ex:
            self.assertEqual(ex.value, CKR_USER_ALREADY_LOGGED_IN)

    def test_list_certs_no_open_session(self):
        try:
            self.tm.list_certs(self.slot_serial)
            self.assertEqual(1, 0)
        except LogicException as ex:
            self.assertEqual(str(ex), 'Slot is not open.')

    def test_list_certs_not_logged(self):
        self.tm.open_session(self.slot_serial)
        try:
            self.tm.list_certs(self.slot_serial)
            self.assertEqual(1, 0)
        except LogicException as ex:
            self.assertEqual(str(ex), 'Slot is not logged in.')

    def test_list_certs(self):
        self.tm.open_session(self.slot_serial)
        self.tm.login_session(self.slot_serial, self.pin)
        result = self.tm.list_certs(self.slot_serial)
        self.assertEqual(1, len(result))
        self.assertTrue(self.cert_id in result)

    def test_sign_with_cert(self):
        self.tm.open_session(self.slot_serial)
        self.tm.login_session(self.slot_serial, self.pin)
        signed = self.tm.sign_with_cert(self.slot_serial, self.cert_id, 'hello')
        signature = ''.join(['%02x' % i for i in signed])
        self.assertEqual('675d542ae3b11fd3d099201775df780b307eee176a24b51e3242958d8a8ae1cdf09dd8b678981ae4dfdfef2cbc2b759510d65fa45eb984a614bf52ad143df940dace187986c49093563fbda60d397a42f9317bdf60cd9c00c332d7349c1a6749b15f15da2e7b20cddb34e6d59bb6df2260b745ec2ed68fd9661626182b0812c84c962b5c1e7b1209a886dd98a2e40ccfe19ccea298c6bb078b0a45f9cd05f9ae58a275c01f49b69bc65e67a842df6933e8ac05292e136ad6de32155c48315fa42e76cab6766d367ea814748a2759ea88c5bc525da35a1f18275dc1e5db878125d114572e759fc7b2af809a86394ca0ba425ff02e5219b648c635a0bfcdb6f926', signature)
        self.assertEqual(512, len(signature))