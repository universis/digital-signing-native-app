import unittest
import gc, time
from .testTokenManager import file_callback
from src.TokenManager import TokenManager
from src.LogicException import LogicException
from src.CommandRouter import CommandRouter

class InterfaceMock:
    def __init__(self, pin):
        self.pin = pin

    def get_pin(self):
        return self.pin

    def get_library_location(self, loaded, filetypes, pkcs11):
        return False

    def show_error(self, message):
        print(message)

    def ask_permission(self, websocket):
        return True

class TestCommandRouter(unittest.TestCase):
    def setUp(self):
        gc.collect()
        self.tm = TokenManager(file_callback)
        self.tokens_len = 1
        self.slot_serial = '021f60d0'
        self.pin = 'admin'
        self.cert_id = '228.10.12.161.68.68.63.246'
        self.interface = InterfaceMock(self.pin)
        self.cr = CommandRouter(self.tm, self.interface)
        # IMPORTANT START
        # THIS IS BECAUSE GC is not fast enough
        # the sessions do not close as a result
        # we see no devices
        time.sleep(0.5)
        # MODIFY ACCORDING NEEDS
        # IMPORTANT END

    def tearDown(self):
        gc.collect()

    def test_help_command(self):
        result = self.cr.route({
            'command': 'help'
        })
        self.assertTrue('command' in result)
        self.assertTrue('payload' in result)
        self.assertTrue('list' in result['payload'])

    def test_request_id_command(self):
        result = self.cr.route({
            'command': 'help',
            'request_id': 100
        })
        self.assertTrue('command' in result)
        self.assertTrue('payload' in result)
        self.assertTrue('request_id' in result)
        self.assertTrue(result['request_id'] == 100)
        self.assertTrue('list' in result['payload'])

    def test_list_slots_command(self):
        result = self.cr.route({
            'command': 'list_slots'
        })
        payload = result['payload']
        self.assertTrue('command' in result)
        self.assertTrue('payload' in result)
        self.assertTrue(self.slot_serial in payload)

    def test_open_session_missing_slot_serial_command(self):
        result = self.cr.route({
            'command': 'open_session'
        })
        self.assertTrue('command' in result)
        self.assertTrue('error' in result)
        self.assertFalse('payload' in result)
        self.assertTrue('Missing' in result['error'])

    def test_open_session_invalid_slot_serial_command(self):
        result = self.cr.route({
            'command': 'open_session',
            'slot_serial': '404'
        })
        self.assertTrue('command' in result)
        self.assertTrue('error' in result)
        self.assertFalse('payload' in result)
        self.assertEqual('Incorrect slot serial number.', result['error'])

    def test_open_session_command(self):
        result = self.cr.route({
            'command': 'open_session',
            'slot_serial': self.slot_serial
        })
        payload = result['payload']
        self.assertTrue('command' in result)
        self.assertTrue('payload' in result)
        self.assertTrue('message' in payload)

    def test_login_session_command(self):
        self.test_open_session_command()
        result = self.cr.route({
            'command': 'login_session',
            'slot_serial': self.slot_serial
        })
        payload = result['payload']
        self.assertTrue('command' in result)
        self.assertTrue('payload' in result)
        self.assertTrue('message' in payload)

    def test_list_certs_command(self):
        self.test_open_session_command()
        self.test_login_session_command()
        result = self.cr.route({
            'command': 'list_certs',
            'slot_serial': self.slot_serial
        })
        payload = result['payload']
        self.assertTrue('command' in result)
        self.assertTrue('payload' in result)
        self.assertTrue(self.cert_id in payload)

    def test_sign_with_cert_command(self):
        self.test_open_session_command()
        self.test_login_session_command()
        result = self.cr.route({
            'command': 'sign_with_cert',
            'slot_serial': self.slot_serial,
            'cert_id': self.cert_id,
            'message': 'hello',
            'source_encoding': 'plain',
            'destination_encoding': 'hex'
        })
        payload = result['payload']
        self.assertTrue('command' in result)
        self.assertTrue('payload' in result)
        self.assertTrue('signature' in payload)
        signature = payload['signature']
        self.assertEqual(512, len(signature))