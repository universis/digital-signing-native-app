# UniverSIS Digital Signing for Grade Submissions - Native App
This app is used to digitally sign the grade documents that Instructors submit through the UniverSIS Teacher web application. 
It has to be started on the Instructor's PC (cross-platform support for Windows, Linux or MacOSX) and opens a listening server (localhost port) 
that allows communication with the user's browser, which is running the web application, in order to allow the browser to communicate with the native OS 
and the security token that holds the Instructor's certificate.

## Implementation
The app is written in python (requires v3.5 or newer for asyncio use) and depends on
* tkinter - Tk graphical interface
* websockets - library to expose a localhost interface to the browser
* cryptography - library to implement basic cryptographic functions
* pykcs11 - library to abstract the communication with security token, the hardware device that stores the user's digital certificate
* pyinstaller - library to allow building the executables for cross-platform support

## Requirements
The pkcs11 library assumes the appropriate drivers for interfacing with the security devices are already installed. The paths for loading the libraries are hardwired in the native app code:
```
Native App$ less src/TokenManager.py
    def get_libraries(self):
        libraries = []
        filetypes = []
        if self.platform == 'linux':
            libraries = ["/usr/lib/libeTPkcs11.so", "/usr/lib/pkcs11/libgclib.so"]
            filetypes = [("Shared objects", "*.so")]
        elif self.platform == 'win32':
            libraries = ["C:\\Windows\\System32\\eTPKCS11.dll", "C:\\Program Files (x86)\\Gemalto\\Classic Client\\BIN\\gclib.dll", "C:\\Program Files\\Gemalto\\Classic Client\\BIN\\gclib.dll"]
            filetypes = [("Dynamic Link Libraries", "*.dll")]
        elif self.platform == 'darwin':
            libraries = ["/usr/local/lib/libeTPkcs11.dylib", "/usr/local/lib/ClassicClient/libgclib.dylib"]
            filetypes = [("Dynamic Libraries", "*.dylib")]
```
Failure to load all pkcs11 drivers, causes the app to prompt the user to enter an alternative path to an installed pkcs11 driver shared lib.

## Gemalto support

### Windows
Install [package](https://supportportal.gemalto.com/csm/?sys_kb_id=68db1c5edb9bbe40d298728dae9619e9&id=kb_article_view&sysparm_rank=1&sysparm_tsqueryId=1c50a1101b378094f12064606e4bcbe1) from official support site.


### MacOSX
Install [package](https://supportportal.gemalto.com/csm/?sys_kb_id=befd649adb1fbe40d298728dae961991&id=kb_article_view&sysparm_rank=2&sysparm_tsqueryId=c05f559c1bf38094f12064606e4bcbd0
) from official support site.

### Linux
We have in depth installation guides for the 3 distributions where official Gemalto drivers exist.

You can find them here:

<a href="installation-guide/Ubuntu.INSTALL.md#getting-started"><img height="64px" src="https://assets.ubuntu.com/v1/29985a98-ubuntu-logo32.png" alt="Ubuntu"></a>
<a href="installation-guide/Fedora.INSTALL.md#getting-started"><img height="64px" src="https://fedoraproject.org/w/uploads/6/65/Fedora_vertical.png" alt="Fedora"></a>
<a href="installation-guide/openSUSE.INSTALL.md#getting-started"><img height="64px" src="https://en.opensuse.org/images/c/cd/Button-colour.png" alt="openSUSE"></a>

A successful installation will make the following pkcs11 library available for our python application to load
```
/usr/lib/pkcs11/libgclib.so
```

## Server commands
The native app runs a local webserver which responds to the following commands
* help - lists the commands supported
* list_slots - lists the security devices (slots) that the pkcs11 library is able to communicate with
* login_session - login with pin to open a specific device (slot)
* list_certs - lists the certificates contained in a specific device (slot)
* open_session - opens a session for interacting with a specific slot and certificate
* sign_with_cert - utilizes an active session to sign a payload with a specific certificate

## Sample messages
Sample messages exchanged with the server (in json) can be seen by running the following demo client python app. What follows is the dialog of commands and payloads returned. Make sure the slot serials and cert numbers hardwired in the client code are updated to match your personal setup, i.e. specific token device and personal cert stored. Running the script the first time, displays both by querying the local setup and listing them in the native app payload responses.
```
Native App$ pipenv run python ./examples/python/client.py
< {"command": "help"}
> {"payload": {"list": ["help", "list_slots", "open_session", "login_session", "list_certs", "sign_with_cert"]}, "command": "help"}

# list all devices available. Each one is assigned a unique slot number, which should be used to address them from here on
< {"command": "list_slots"}
> {"payload": {"A678FG657610BC09": {"label": "GemP15-1", "manufacturerID": "Gemalto S.A.", "open": true, "logged": true, "certificates":
{"123.123...123.123": {"raw": "MII...e8q", "info": {"issuer": "C=GR,O=Aristotle University of Thessaloniki,CN=Aristotle University of Thessaloniki Central CA", "subject": "C=GR,L=Thessaloniki,O=Aristotle University of Thessaloniki,OU=IT Center,OU=Class A - Private Key created and stored in hardware CSP,2.5.4.4=Dokimastikidis,2.5.4.42=Dokimos,2.5.4.5=6792477117,CN=Dokimos Dokimastikidis,1.2.840.113549.1.9.1=dokimos@auth.gr", "serial_number": "123456789", "version": "Version.v3", "not_valid_after": "2022-07-29 04:27:17", "not_valid_before": "2020-07-29 04:27:17", "public_key": {"size": "2048", "key": "-----BEGIN PUBLIC KEY-----\nMI...QAB\n-----END PUBLIC KEY-----\n"}}}}}}, "command": "list_slots"}

# τry to list all certs stored in the specfic slot, but slot is not open yet
< {"command": "list_certs", "slot_serial": "A678FG657610BC09"}
> {"error": "Slot is not open.", "command": "list_certs"}

# open session to the specific slot
< {"command": "open_session", "slot_serial": "A678FG657610BC09"}
> {"payload": {"message": "Opened session."}, "command": "open_session"}

# try again to list all certs stored in the specific slot, now that a session is opened, but it requires logging in
< {"command": "list_certs", "slot_serial": "A678FG657610BC09"}
> {"error": "Slot is not logged in.", "command": "list_certs"}

# try to login (the native app pops up a window for entering PIN to unlock slot)
< {"command": "login_session", "slot_serial": "A678FG657610BC09"}
> {"payload": {"message": "Successful logged in."}, "command": "login_session"}

# try to login again and see already logged in message
< {"command": "login_session", "slot_serial": "A678FG657610BC09"}
> {"payload": {"message": "Already logged in."}, "command": "login_session"}

# finally list available certificates again
< {"command": "list_certs", "slot_serial": "A678FG657610BC09"}
> {"payload": {"123.123...123.123": {"raw": "MII...e8q", "info": {"issuer": "C=GR,O=Aristotle University of Thessaloniki,CN=Aristotle University of Thessaloniki Central CA", "subject": "C=GR,L=Thessaloniki,O=Aristotle University of Thessaloniki,OU=IT Center,OU=Class A - Private Key created and stored in hardware CSP,2.5.4.4=Dokimastikidis,2.5.4.42=Dokimos,2.5.4.5=6792477117,CN=Dokimos Dokimastikidis,1.2.840.113549.1.9.1=dokimos@auth.gr", "serial_number": "123456789", "version": "Version.v3", "not_valid_after": "2022-07-29 04:27:17", "not_valid_before": "2020-07-29 04:27:17", "public_key": {"size": "2048", "key": "-----BEGIN PUBLIC KEY-----\nMI...QAB\n-----END PUBLIC KEY-----\n"}}}}}}, "command": "list_certs"}

# try to list certs with the wrong slot serial
< {"command": "list_certs", "slot_serial": "1111111111111111"}
> {"error": "Incorrect slot serial number.", "command": "list_certs"}

# try to list certs with the correct slot serial and check status of slot as well as list of certs
< {"command": "list_slots"}
> {"payload": {"5780011C2A9A3873": {"label": "GemP15-1", "manufacturerID": "Gemalto S.A.", "open": true, "logged": true, "certificates":
{"123.123...123.123": {"raw": "MII...e8q", "info": {"issuer": "C=GR,O=Aristotle University of Thessaloniki,CN=Aristotle University of Thessaloniki Central CA", "subject": "C=GR,L=Thessaloniki,O=Aristotle University of Thessaloniki,OU=IT Center,OU=Class A - Private Key created and stored in hardware CSP,2.5.4.4=Dokimastikidis,2.5.4.42=Dokimos,2.5.4.5=6792477117,CN=Dokimos Dokimastikidis,1.2.840.113549.1.9.1=dokimos@auth.gr", "serial_number": "123456789", "version": "Version.v3", "not_valid_after": "2022-07-29 04:27:17", "not_valid_before": "2020-07-29 04:27:17", "public_key": {"size": "2048", "key": "-----BEGIN PUBLIC KEY-----\nMI...QAB\n-----END PUBLIC KEY-----\n"}}}}}}, "command": "list_slots"}

# try to sign something with a specific certificate id in the slot identified
< {"command": "sign_with_cert", "slot_serial": "A678FG657610BC09", "cert_id": "123.123...123.123", "message": "iqqcKxuFJeb6TAg2Tm3E9DAOySw=", "source_encoding": "base64", "destination_encoding": "base64"}
> {"payload": {"signature": "j0C...Baw=="}, "command": "sign_with_cert"}
```

## Building native executables
NOTE: Native Executables are self contained.
They DO NOT need python installed on the machine.
Pre-built executables may be found in the artifacts folder.

## Build Environment Requirements
Disclaimer: I removed the lock file for environment inconsistencies across operating systems. Please build with caution.

### Windows
1. Install Python 3.6+ and pip
2. Install [Swig](http://www.swig.org/download.html) & add to path
3. Install [Buildtools C++](https://go.microsoft.com/fwlink/?LinkId=691126)
4. `pip install --user pipenv`
5. `pipenv install`
6. `.\scripts\build_windows.bat`

### Linux
1. `sudo apt install python3-pip`
2. `sudo apt install build-essential gcc python3-dev python3-setuptools swig python3-tk`
3. `pip3 install --user pipenv`
4. add `export PATH="$HOME/.local/bin:$PATH"` to the end of your `.bashrc`
5. rename `LinuxPipfile.lock` to `Pipfile.lock`
6. `pipenv sync`
7. `./scripts/build_linux.sh`

### macOS
1. `brew install python3`
2. `pip3 install --user pipenv`
3. `brew install swig`
4. `pipenv install`
5. `sh ./scripts/build_macos.sh`

## Development Environment Requirements

### Windows Environment
1. Install python 3.6+ and pip & add to path
2. [Swig](http://www.swig.org/download.html) & add to path
3. [Buildtools C++](https://go.microsoft.com/fwlink/?LinkId=691126)
4. `pip install --user pipenv`
5. `pipenv install`

### Linux Environment
We have in depth installation guides for the 3 distributions where official Gemalto drivers exist.

You can find them here:

<a href="installation-guide/Ubuntu.INSTALL.md#development-guides"><img height="64px" src="https://assets.ubuntu.com/v1/29985a98-ubuntu-logo32.png" alt="Ubuntu"></a>
<a href="installation-guide/Fedora.INSTALL.md#development-guides"><img height="64px" src="https://fedoraproject.org/w/uploads/6/65/Fedora_vertical.png" alt="Fedora"></a>
<a href="installation-guide/openSUSE.INSTALL.md#development-guides"><img height="64px" src="https://en.opensuse.org/images/c/cd/Button-colour.png" alt="openSUSE"></a>

### macOS Environment
1. `brew install python3`
2. `pip3 install --user pipenv`
3. `brew install swig`
4. `pipenv install`

## Development Run App
1. `pipenv install`
2. `pipenv run python main.py`

## Testing
IMPORTANT: the tests require a crypto-token device.
1. `pipenv run python -m unittest discover`

## Examples

### Basic example
Simply serve the index.html file

### Angular example
1. `npm i`
2. `npm run start`
3. Browse to http://localhost:4200

### Python Example
1. `pipenv run ./examples/python/client.py