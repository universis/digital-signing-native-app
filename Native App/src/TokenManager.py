import PyKCS11

from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.serialization import Encoding, PublicFormat

import base64
import hashlib
import json
from datetime import datetime
from .LogicException import LogicException
from .FatalException import FatalException
import sys, os
from .logger import default_logger


def ints_to_str(ints):
    return '.'.join(str(x) for x in ints)


def str_to_ints(string):
    return tuple([int(x) for x in string.split('.')])


'''
Used to provide a high level abstraction of the TokenManager interface
'''


class TokenManager:
    '''
        Used to initialize a TokenManager class

        You can supply a function at library_callback parameter
        The function accepts the following parameters:
        * loaded <boolean> if the pkcs11 loaded any of the defined libraries
        * pkcs11 <object> you can use it to load custom libraries if you want
    '''

    def __init__(self, library_callback=None):
        # telemetry variables
        self.platform = sys.platform
        self.libraries, self.filetypes = self.get_libraries()

        # logic variables
        self.pkcs11 = []

        # State
        self.lib_loaded = False
        self.slots_info = {}

        # initial proccess
        self.lib_loaded = self._init_pcks11(library_callback)

    def get_libraries(self):
        libraries = []
        filetypes = []
        if self.platform == 'linux':
            libraries = ["/usr/lib/libeTPkcs11.so", "/usr/lib/pkcs11/libgclib.so"]
            filetypes = [("Shared objects", "*.so")]
        elif self.platform == 'win32':
            libraries = ["C:\\Windows\\System32\\eTPKCS11.dll",
                         "C:\\Program Files (x86)\\Gemalto\\Classic Client\\BIN\\gclib.dll",
                         "C:\\Program Files\\Gemalto\\Classic Client\\BIN\\gclib.dll"]
            filetypes = [("Dynamic Link Libraries", "*.dll")]
        elif self.platform == 'darwin':
            libraries = ["/usr/local/lib/libeTPkcs11.dylib", "/usr/local/lib/ClassicClient/libgclib.dylib"]
            filetypes = [("Dynamic Libraries", "*.dylib")]

        return libraries, filetypes

    def _init_pcks11(self, library_callback):
        loaded = False
        for library in self.libraries:
            if not os.path.exists(library):
                default_logger.error("Library " + library + " doesn't exist")
                print('Library does not exist:', library)
                continue
            try:
                pkcsInterface = PyKCS11.PyKCS11Lib()
                pkcsInterface.load(library)
                self.pkcs11.append(pkcsInterface)
                print('Loaded library:', library)
                loaded = True
                default_logger.debug("Library " + str(library) + " has been loaded successfully")
            except PyKCS11.PyKCS11Error as e:
                print('Library load error:', library, ' - ', e)
                default_logger.error("Library " + library + " failed to load." + str(e))
                continue
        # if library loader callback is defined use it.
        # used to load library from interface
        if library_callback:
            pkcsInterface = PyKCS11.PyKCS11Lib()
            loaded = library_callback(loaded, self.filetypes, pkcsInterface)
            if (loaded):
                self.pkcs11.append(pkcsInterface)
        return loaded

    '''
        Used to list all the available crypto token slots of the machine
    '''

    def list_slots(self):
        self.slots_info = self._list_slots(self.slots_info)
        slots_dict = {}
        for key, info in self.slots_info.items():
            slots_dict[key] = self._filter_info(info, ['label', 'manufacturerID', 'open', 'logged', 'certificates'])
        return slots_dict

    def _filter_info(self, info, keys):
        filtered_dict = {my_key: info[my_key] for my_key in keys}
        return filtered_dict

    def _list_slots(self, slots_info):
        for library in self.pkcs11:
            slots = None
            try:
                slots = library.getSlotList()
            except PyKCS11.PyKCS11Error as e:
                default_logger.error('get_token_info_error: ' + str(type(e)) + " " + str(e))
                print('get_slot_list_error', type(e), e)
                continue
            for slot in slots:
                try:
                    info = None
                    try:
                        info = library.getTokenInfo(slot).to_dict()
                    except PyKCS11.PyKCS11Error as e:
                        default_logger.error('get_token_info_error: ' + str(type(e)) + " " + str(e))
                        print('get_token_info_error', type(e), e)
                        continue
                    label = info['label'].strip()
                    serialNumber = info['serialNumber'].strip()
                    manufacturerID = info['manufacturerID'].strip()
                    if serialNumber not in slots_info:
                        slots_info[serialNumber] = {
                            'label': label,
                            'manufacturerID': manufacturerID,
                            'open': False,
                            'logged': False,
                            'certificates': {},
                            'slot': slot,
                            'library': library
                        }
                except PyKCS11.PyKCS11Error as e:
                    if e.value == PyKCS11.CKR_TOKEN_NOT_PRESENT:
                        default_logger.error("Cryptographic device is either absent or not supported.")
                        continue
                    elif e.value == PyKCS11.CKR_DEVICE_REMOVED:
                        default_logger.error("Cryptographic device was removed.")
                        continue
                    print('list_info', str(type(e)), e)
                    continue
                except TypeError as typeError:
                    default_logger.error("A type Error occurred while listing slots." + str(typeError))
        return slots_info

    '''
        Used to lock a session with the defined slot

        throws:
        * LogicException
        * PyKCS11.PyKCS11Error
    '''

    def open_session(self, slot_serial):
        self._check_slots_empty()
        self._check_slot_serial(slot_serial)
        self.slots_info[slot_serial] = self._open_session(self.slots_info[slot_serial])
        default_logger.debug("Opened session successfully. Locking the session with the defined slot.")
        return True

    def _open_session(self, slot_info):
        slot = slot_info['slot']
        library = slot_info['library']
        session = library.openSession(slot)
        slot_info['session'] = session
        slot_info['open'] = True
        return slot_info

    '''
        Used to log into a slot

        throws:
        * LogicException
        * PyKCS11.PyKCS11Error: For invalid PIN
    '''

    def login_session(self, slot_serial, pin):
        self._check_slots_empty()
        self._check_slot_serial(slot_serial)
        self._check_slot_open(slot_serial)
        self.slots_info[slot_serial] = self._login_session(self.slots_info[slot_serial], pin)
        default_logger.debug("Successfully logged in.")
        return True

    def _login_session(self, slot_info, pin):
        slot_info['session'].login(pin=pin)
        slot_info['logged'] = True
        return slot_info

    '''
        Used to list all available certificates of a slot

        throws:
        * LogicException
    '''

    def list_certs(self, slot_serial):
        self._check_slots_empty()
        self._check_slot_serial(slot_serial)
        self._check_slot_open(slot_serial)
        self._check_slot_logged(slot_serial)
        self.slots_info[slot_serial] = self._list_certs(self.slots_info[slot_serial])
        return self.slots_info[slot_serial]['certificates']

    def _list_certs(self, slot_info):
        certificates = slot_info['session'].findObjects([(PyKCS11.CKA_CLASS, PyKCS11.CKO_CERTIFICATE)])
        for certificate in certificates:
            certificate_dict = certificate.to_dict()
            certificate_id = certificate_dict['CKA_ID']
            if certificate_id not in slot_info['certificates']:
                # TODO breakdown function
                cert_info = base64.b64encode(bytes(certificate_dict["CKA_VALUE"])).decode("utf8")
                pem = ('-----BEGIN CERTIFICATE-----\n' + cert_info + '\n-----END CERTIFICATE-----\n').encode('ascii')
                cert = x509.load_pem_x509_certificate(pem, default_backend())
                slot_info['certificates'][ints_to_str(certificate_id)] = {
                    'raw': cert_info,
                    'info': {
                        'issuer': str(cert.issuer.rfc4514_string()),
                        'subject': str(cert.subject.rfc4514_string()),
                        'serial_number': str(cert.serial_number),
                        'version': str(cert.version),
                        'not_valid_after': str(cert.not_valid_after),
                        'not_valid_before': str(cert.not_valid_before),
                        'public_key': {
                            'size': str(cert.public_key().key_size),
                            'key': str(cert.public_key().public_bytes(Encoding.PEM, PublicFormat.SubjectPublicKeyInfo),
                                       'utf-8'),
                        }
                    }
                }
            else:
                continue
        return slot_info

    '''
        Used to sign the message using the certificate of the defined slot

        throws:
        * LogicException
        * FatalException
    '''

    def sign_with_cert(self, slot_serial, cert_id, message):
        self._check_slots_empty()
        self._check_slot_serial(slot_serial)
        self._check_slot_open(slot_serial)
        self._check_slot_logged(slot_serial)
        slot_info = self.slots_info[slot_serial]
        if len(slot_info['certificates']) == 0:
            self.list_certs(slot_serial)
        if cert_id not in slot_info['certificates']:
            default_logger.critical("Invalid certificate ID. Aborting...")
            raise LogicException('Invalid certificate ID.')
        return self._sign_with_cert(slot_info, str_to_ints(cert_id), message)

    '''
        Private function tha finds the privatekey
        handler for the given certificate on
        the defined slot and signs the message payload

        throws:
        * LogicException
        * FatalException
    '''

    def _sign_with_cert(self, slot_info, cert_id, message):
        private_keys = slot_info['session'].findObjects(
            [(PyKCS11.CKA_CLASS, PyKCS11.CKO_PRIVATE_KEY), (PyKCS11.CKA_ID, cert_id)])
        if len(private_keys) != 1:
            default_logger.critical("Couldn\'t find the private key for this certificate")
            raise LogicException('Could not find a single private key for this certificate')
        private_key = private_keys[0]
        if private_key is None:
            default_logger.critical("Something went wrong while reading the private certificate key. Raising Fatal "
                                    "Exception.")
            raise FatalException(
                'Something went wrong. Please report this incident at support@it.auth.gr - code #C57AFD')
        return slot_info['session'].sign(private_key, message)

    def cleanup(self):
        for key in self.slots_info:
            slot = self.slots_info[key]
            if 'session' in slot:
                slot['session'].closeSession()
        self.pkcs11 = []
        self.lib_loaded = False
        self.slots_info = {}

    def _check_slots_empty(self):
        if (len(self.slots_info) == 0):
            self.list_slots()
        if (len(self.slots_info) == 0):
            default_logger.error("No connected cryptographic token found.")
            raise LogicException('There are no connected token devices. Please connect one and try again.')

    def _check_slot_serial(self, slot_serial):
        if slot_serial not in self.slots_info:
            default_logger.error("The slot serial number is incorrect. Aborting")
            raise LogicException('Incorrect slot serial number.')

    def _check_slot_open(self, slot_serial):
        if not self.slots_info[slot_serial]['open']:
            default_logger.error("The slot isn't open")
            raise LogicException('Slot is not open.')

    def _check_slot_logged(self, slot_serial):
        if not self.slots_info[slot_serial]['logged']:
            default_logger.error("The slot isn't logged in")
            raise LogicException('Slot is not logged in.')

    def session_logout(self, slot_serial):
        self._check_slots_empty()
        self._check_slot_serial(slot_serial)
        slot_info = self.slots_info[slot_serial]
        if slot_info is not None:
            if slot_info['logged']:
                slot_info['open'] = False
                slot_info['session'].logout()
                slot_info['logged'] = False


if __name__ == "__main__":
    tm = TokenManager()
    print(tm.list_slots())
    tm.open_session('021f60d0')
    print(tm.list_slots())
    tm.login_session('021f60d0', 'admin')
    print(tm.list_slots())
    print(tm.list_certs('021f60d0'))
    print(tm.sign_with_cert('021f60d0', '228.10.12.161.68.68.63.246', 'hello'))
    print(tm.list_slots())
