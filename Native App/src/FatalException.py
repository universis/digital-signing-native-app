'''
Represent out of bounds application behavior.
'''


class FatalException(Exception):
    pass
