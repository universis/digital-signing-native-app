from .LogicException import LogicException
import base64
import PyKCS11
import sys
from .logger import default_logger

'''
Routing Component
Used to route the commands to the appropriate bussiness logic function.
Designed in mind with ease of extensibility.
'''


class CommandRouter:
    def __init__(self, token_manager, interface):
        self.commands = {
            'help': self.help,
            'list_slots': self.list_slots,
            'open_session': self.open_session,
            'login_session': self.login_session,
            'list_certs': self.list_certs,
            'sign_with_cert': self.sign_with_cert,
            'close_session': self.close_session
        }

        self.token_manager = token_manager
        self.interface = interface

    def route(self, payload):
        command = self._parse_param('command', payload)

        request_id = None
        if 'request_id' in payload:
            request_id = payload['request_id']

        response = {'error': 'Something went wrong. Please report this incident.', 'command': command}
        if command not in self.commands:
            response = {'error': f'Invalid command "{command}". Send "help" for list of commands.', 'command': command}
        else:
            try:
                response = {'payload': self.commands[command](payload), 'command': command}
            except LogicException as e:
                default_logger.error("There was an error " + str(e) + " trying to execute " + str(command))
                response = {'error': str(e), 'command': command}
            except Exception as ex:
                response = {'error': str(ex), 'command': command, 'type': str(type(ex))}

        if request_id is not None:
            response['request_id'] = request_id

        return response

    def help(self, payload):
        return {
            'list': list(self.commands.keys())
        }

    def list_slots(self, payload):
        return self.token_manager.list_slots()

    def open_session(self, payload):
        slot_serial = self._parse_param('slot_serial', payload)
        self.token_manager.open_session(slot_serial)
        default_logger.debug("CommandRouter: The session has been opened successfully")
        return {'message': 'Opened session.'}

    def login_session(self, payload):
        slot_serial = self._parse_param('slot_serial', payload)

        slot_info = self.list_slots(payload)[slot_serial]
        if slot_info['logged']:
            return {'message': 'Already logged in.'}

        count = 0
        while True:
            count += 1
            if (count > 10):
                default_logger.error("Too many tries have been made opening logging in the session. Aborting.")
                self.interface.show_error('Too many tries. Closing application.')
                sys.exit(1)
            try:
                pin = self.interface.get_pin()
                self.token_manager.login_session(slot_serial, pin)
                default_logger.debug("Successfully logged in")
                return {'message': 'Successful logged in.'}
            except PyKCS11.PyKCS11Error as e:
                if e.value == PyKCS11.CKR_PIN_INCORRECT:
                    default_logger.debug("User entered an invalid pin, prompting to try again.")
                    self.interface.show_error('Invalid pin. Please try again.')
                elif e.value == PyKCS11.CKR_USER_ALREADY_LOGGED_IN:
                    default_logger.debug("The user is already logged in. Continuing.")
                    return {'message': 'Already logged in.'}
                else:
                    return {'error': str(e)}

    def list_certs(self, payload):
        slot_serial = self._parse_param('slot_serial', payload)
        return self.token_manager.list_certs(slot_serial)

    def sign_with_cert(self, payload):
        slot_serial = self._parse_param('slot_serial', payload)
        cert_id = self._parse_param('cert_id', payload)
        message = self._parse_param('message', payload)

        source_encoding = self._parse_param('source_encoding', payload)
        destination_encoding = self._parse_param('destination_encoding', payload)
        default_logger.debug("Encoding from " + source_encoding + " to " + destination_encoding)

        if source_encoding == 'base64':
            message = base64.b64decode(message)
        elif source_encoding == 'base32':
            message = base64.b32decode(message)
        elif source_encoding == 'base16':
            message = base64.b16decode(message)
        elif source_encoding == 'plain':
            message = bytes(message, 'utf8')
        else:
            raise LogicException('Invalid source encoding. Available encodings [plain, base64, base32, base16]')

        # if source_hashing == "md5":
        #     plain = hashlib.md5(plain).digest()
        # elif source_hashing == "sha1":
        #     plain = hashlib.sha1(plain).digest()
        # elif source_hashing == "sha256":
        #     plain = hashlib.sha256(plain).digest()
        # elif source_hashing == "sha384":
        #     plain = hashlib.sha384(plain).digest()
        # elif source_hashing == "sha512":
        #     plain = hashlib.sha512(plain).digest()
        # elif source_hashing == "none":
        #     pass
        # else:
        #     send_err("Invalid hash algorithm specified")
        #     return

        signed = self.token_manager.sign_with_cert(slot_serial, cert_id, message)

        if destination_encoding == 'base64':
            signature = base64.b64encode(bytes(''.join([chr(i) for i in signed]), 'latin1')).decode('utf8')
        elif destination_encoding == 'base32':
            signature = base64.b32encode(bytes(''.join([chr(i) for i in signed]), 'latin1')).decode('utf8')
        elif destination_encoding == 'base16':
            signature = base64.b16encode(bytes(''.join([chr(i) for i in signed]), 'latin1')).decode('utf8')
        elif destination_encoding == 'hex':
            signature = ''.join(['%02x' % i for i in signed])
        else:
            raise LogicException('Invalid destination encoding. Available encodings [hex, base64, base32, base16]')

        if self.interface.launch_gui is False:
            self.interface.open_gui = False
            self.interface.destroy_gui()
        return {
            'signature': signature
        }

    def _parse_param(self, param, payload):
        if param not in payload:
            raise LogicException(f'Missing "{param}" payload parameter')
        return payload[param]

    def _return_error_json(self, message):
        return {'error': message}

    def close_session(self, payload):
        slot_serial = self._parse_param('slot_serial', payload)
        self.token_manager.session_logout(slot_serial)
        default_logger.debug("CommandRouter: The session was closed successfully")
        if sys.platform != 'win32':
            return {'message': 'Closed session.'}
        else:
            self.interface.quit_application()
