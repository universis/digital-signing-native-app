import getpass
import tkinter
from tkinter import simpledialog, messagebox, filedialog
import asyncio
import os
import sys, traceback
from .logger import default_logger, stopLogging
'''
Helper function used to parse the url get params
'''


def parse_path_params(path):
    path = path.split('?')[-1]
    path_pairs = [pair.split('=') for pair in path.split('&')]
    path_params = {pair[0]: pair[1] for pair in path_pairs}
    return path_params


class BasicInterfaceController:
    def get_pin(self):
        return getpass.getpass('Enter PIN:')

    def get_library_location(self, loaded, filetypes, pkcs11):
        if not loaded:
            print('Loaded', loaded)

            path = input('Enter library path:')
            print(path)
            return False
        return True

    def show_error(self, message):
        default_logger.debug(message)
        print(message)

    def ask_permission(self, websocket):
        path_params = parse_path_params(websocket.path)
        if 'app' not in path_params:
            message = 'Missing "app" parameter.'
            default_logger.debug(message)
            return message
        allowed = input(f'Do you want the application <{path_params["app"]}> to connect (y/n): ')
        if allowed == 'y':
            default_logger.debug('Connect to application...')
            return True
        else:
            default_logger.error('Permission denied')
            return 'Application not allowed to connect.'


'''
Used to abstract the interface API from the rest application
'''


class GraphicalInterface:
    def __init__(self, launch_gui=True):
        self.open_gui = False
        self.launch_gui = launch_gui
        self.root = None
        if self.launch_gui is True and not self.open_gui:
            self._draw_gui()

    def _draw_gui(self):
        if not self.open_gui:
            root = tkinter.Tk()
            root.title('UniverSIS - Digital Signing for Grades')
            root.geometry("400x100")
            frame = tkinter.Frame(root, height=80, width=230)
            frame.pack(padx=20, pady=20)
            label = tkinter.Label(frame, text='Ready to sign', fg='green')
            label.pack(side=tkinter.TOP)
            self.root = root
            self.open_gui = True

    def destroy_gui(self):
        self.open_gui = False
        self.root = None

    def lift_to_top(self):
        self.root.attributes('-topmost', True)

    def default_to_top(self):
        self.root.attributes('-topmost', False)

    def get_pin(self):
        self.lift_to_top()
        result = simpledialog.askstring('Enter PIN', 'Enter PIN:', show='*')
        self.default_to_top()
        return result

    def get_library_location(self, loaded, filetypes, pkcs11):
        if not loaded:
            while True:
                self.lift_to_top()
                default_logger.debug('Couldn\'t find any pkcs11 libraries, Asking the user to load a custom one')
                ans = messagebox.askyesno('Error',
                                          'Could not load any pkcs11 libraries. Do you want to load a custom library ?')
                if not ans:
                    default_logger.critical('User refused to load a custom library. Quitting...')
                    quit()
                path = filedialog.askopenfilename(initialdir=os.getcwd(), title="Please select a library:",
                                                  filetypes=filetypes)
                if not os.path.exists(path):
                    continue
                try:
                    pkcs11.load(path)
                    self.default_to_top()
                    return True
                except Exception:
                    continue

    def show_error(self, message):
        self.lift_to_top()
        default_logger.error(message)
        messagebox.showerror('Error', message=message)
        self.default_to_top()

    def ask_permission(self, websocket,):
        path_params = parse_path_params(websocket.path)
        if 'app' not in path_params:
            return 'Missing "app" parameter.'
        if self.launch_gui is False:
            self._draw_gui()
        self.lift_to_top()
        default_logger.debug("Found app parameter. Asking for permission.")
        allowed = messagebox.askyesno('Allow Application',
                                      f'Do you want to allow the application <{path_params["app"]}> to connect with Universis Grades Digital Signing and use your digital certificate devices?')
        self.default_to_top()
        if allowed:
            default_logger.info('User gave permission to application')
            return True
        else:
            if self.launch_gui is False:
                self.open_gui = False
                self.destroy_gui()
            default_logger.info("User denied permission to application")
            print('Application not allowed to connect.')
            return False

    '''
    This function in the interface update loop
    The line 'await asynio.sleep(x)' represents the frequency of the interface updates
    '''

    async def loop(self):
        try:
            while True:
                if self.root is not None:
                    self.root.update()
                await asyncio.sleep(0.1)
        except tkinter.TclError as e:
            if "application has been destroyed" not in e.args[0]:
                default_logger.critical(e)
                raise
            else:
                exc_type, exc_value, exc_traceback = sys.exc_info()
                default_logger.exception(e)
                traceback.format_exception(exc_type, exc_value, exc_traceback)
                stopLogging()
                sys.exit(0)
                pass

    '''
       This closes the application
    '''
    def quit_application(self):
        self.root.destroy()
        self.root = None
        sys.exit(0)
