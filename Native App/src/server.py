#!/usr/bin/env python3
import asyncio
import websockets
import json
import sys, getopt
from .TokenManager import TokenManager
from .Interfaces import BasicInterfaceController, GraphicalInterface
from .CommandRouter import CommandRouter
import os
from pid.decorator import pidfile
from pid import PidFileAlreadyLockedError
from .logger import default_logger

# interface = BasicInterfaceController()
launch_gui = True
try:
    options, args = getopt.getopt(sys.argv[1:], shortopts="", longopts=["launch_gui="])
except getopt.GetoptError as err:
    default_logger.error("Option is not recognized" + str(err))
    print(err)  # will print something like "option -a not recognized"
    sys.exit(2)
for opt, arg in options:
    if opt == "--launch_gui":
        # options from CLI are always of type str. This typecasts it to
        # an integer and then to boolean
        launch_gui = bool(int(arg))
interface = GraphicalInterface(launch_gui)
token_manager = TokenManager(interface.get_library_location)
command_router = CommandRouter(token_manager, interface)


async def request_loop(websocket, path, logger=default_logger):
    allowed = interface.ask_permission(websocket)
    if not allowed:
        await websocket.send(json.dumps({'error': allowed}))
        return websocket.close(1000)
    try:
        async for message in websocket:
            default_logger.debug('> ', message)
            try:
                input_payload = json.loads(message)
                output_payload = command_router.route(input_payload)
            except json.decoder.JSONDecodeError:
                output_payload = {'error': 'Invalid payload JSON.'}
            await websocket.send(json.dumps(output_payload))
            default_logger.debug('< ', json.dumps(output_payload))
    except websockets.exceptions.ConnectionClosed:
        logger.debug("ConnectionClosed: Connection closed in request loop", )
        pass  # disconnect
    except Exception as e:
        logger.debug(e)
    finally:
        logger.critical("RequestLoopFinally: Failed to execute.")
        pass  # after something bad


@pidfile()
def main(logger=default_logger):
    try:
        signing_server = websockets.serve(request_loop, 'localhost', 8765, max_queue=2 ** 20,
                                          max_size=2**20, read_limit=2*65536, write_limit=2*65536)  # port number is here
        logger.debug('Native Signing Application started on port 8765')
        logger.debug('Starting event loop')

        async def looper():
            await asyncio.gather(signing_server, interface.loop())

        asyncio.get_event_loop().run_until_complete(looper())
        asyncio.get_event_loop().run_forever()
    except PidFileAlreadyLockedError:
        logger.debug("Lock is acquired. Close the running process and try running it again.")
    except Exception as e:
        logger.debug(type(e))
        pass


if __name__ == "__main__":
    main(default_logger)
