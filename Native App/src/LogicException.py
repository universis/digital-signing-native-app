'''
Used to inform the user/developer about trivial logic flaws he/she did.
'''


class LogicException(Exception):
    pass
