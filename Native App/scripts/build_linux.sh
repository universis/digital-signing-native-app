#!/usr/bin/env bash
pipenv run pyinstaller --icon=./icons/icon.ico --onefile -n GradesDigitalSigning.sh main.py --hidden-import=tkinter
